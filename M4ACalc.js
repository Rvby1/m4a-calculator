//variables to reference input elements in the DOM: 
var userCurrencyInputs = { 
    currentHealthcareCostsInput: null, 
    currentPrescriptionCostsInput: null, 
    currentIncomeInput: null,
}
var currentNumDependentsInput = null;
var currentNumChildrenInput = null;
var currentFilingStatusInput = null;

//variables to reference display elements in the DOM: 
var M4ACalcDisplayElements = { 
    healthcareCosts: null, 
    prescriptionCosts: null, 
    healthcareSavings: null, 
}

//data for the M4A calculation: 
const standardDeductionValues = { 
    single: 12600, 
    married: 25200, 
    headOfHousehold: 18950,
}
const standardDeductionPerDependent = 500;
const standardDeductionPerChild = 2000;

const percentageTaxed = 4;
const maxPrescriptionCost = 200;

window.onload = function() { 
    //get the currency-based healthcare inputs and put them in an object: 
    userCurrencyInputs.currentHealthcareCostsInput = document.querySelector("form #current-healthcare-costs");
    userCurrencyInputs.currentPrescriptionCostsInput = document.querySelector("form #current-prescription-costs");
    userCurrencyInputs.currentIncomeInput = document.querySelector("form #current-income");
    for(property in userCurrencyInputs) { 
        userCurrencyInputs[property].addEventListener("input", calculateAndDisplayHealthCosts);
    }

    //get the non-currency-based healthcare inputs: 
    currentNumDependentsInput = document.querySelector("form #num-dependents");
    currentNumDependentsInput.addEventListener("input", calculateAndDisplayHealthCosts);
    currentNumChildrenInput = document.querySelector("form #num-kids");
    currentNumChildrenInput.addEventListener("input", calculateAndDisplayHealthCosts);
    currentFilingStatusInput = document.getElementsByName("marital-status");
    for(let i = 0; i < currentFilingStatusInput.length; i++) { 
        currentFilingStatusInput[i].addEventListener("change", calculateAndDisplayHealthCosts);
    }

    //store references to the paragraphs where we'll print the results of the calculations: 
    M4ACalcDisplayElements.healthcareCosts = document.querySelector("p#m4a-cost");
    M4ACalcDisplayElements.healthcareSavings = document.querySelector("p#m4a-savings");
    this.M4ACalcDisplayElements.prescriptionCosts = document.querySelector("p#m4a-prescription-cost");
}

/**
 * Validates user input, calculates the user's healthcare savings/costs, then displays that information on the page
 */
function calculateAndDisplayHealthCosts() { 
    //format the input field that triggered the event 
    formatInputValueAsCurrency(event.target);

    //if any of the inputs aren't valid, return
    if(!validateM4AFormInputs()) { 
        return;
    }

    //get the user input and sanitize it:  
    var userHealthcareCosts = convertCurrencyToInt(userCurrencyInputs.currentHealthcareCostsInput.value);
    var userPrescriptionCosts = convertCurrencyToInt(userCurrencyInputs.currentPrescriptionCostsInput.value);
    var userIncome = convertCurrencyToInt(userCurrencyInputs.currentIncomeInput.value);
    var userNumDependents = parseInt(currentNumDependentsInput.value); 
    var userNumChildren = parseInt(currentNumChildrenInput.value);
    var taxFilingStatus = getSelectedRadioButtonValue(currentFilingStatusInput);    

    //determine standard deduction: 
    var standardDeduction = calculateStandardDeduction(taxFilingStatus, userNumDependents, userNumChildren);
    
    //running total of M4A cost
    var runningCostOfM4A = 0;

    //find out how much income will be taxed at 4%: 
    var userIncomeTaxable = userIncome - standardDeduction;
    //if the user will have income taxed, find out how much:
    if(userIncomeTaxable > 0) { 
        //cost is equal to 4% of taxable income 
        runningCostOfM4A += userIncomeTaxable / 100 * percentageTaxed;
    }

    //add any healthcare cost savings: 
    var savingsOfM4A = (userHealthcareCosts - runningCostOfM4A);

    //add any prescription savings: 
    var savingsOnPrescriptions = userPrescriptionCosts - maxPrescriptionCost;
    if(savingsOnPrescriptions > 0) { 
        savingsOfM4A += savingsOnPrescriptions;
    }
    var prescriptionCostsUnderM4A = Math.min(userPrescriptionCosts, 200);

    displayM4ACosts(runningCostOfM4A, prescriptionCostsUnderM4A, savingsOfM4A);

    console.log("M4A Cost: " + runningCostOfM4A);
    console.log("M4A Savings: " + savingsOfM4A);
    console.log("Prescriptions Saving: " + savingsOnPrescriptions); 
}

/**
 * Ensures that all inputs on the M4A form are valid
 * @return false if any input is invalid; true otherwise 
 */
function validateM4AFormInputs() { 
    //if any of the healthcare currency inputs 
    for(property in userCurrencyInputs) {
        if(!validateCurrencyInput(userCurrencyInputs[property])) { 
            displayM4ACosts("0", "0", "0");
            return false;
        }
    }
    if(!currentNumDependentsInput.checkValidity()) { 
        return false;
    }
    return true;
}

/**
 * Calculates the standard deduction of a filer 
 * @param {string} taxFilingStatus the filing status of the filer
 * @param {*} numDependents the number of dependents the filer has 
 * @param {*} numChildren the number of children the filer has 
 */
function calculateStandardDeduction(taxFilingStatus, numDependents, numChildren) { 
    var standardDeduction = standardDeductionValues[taxFilingStatus];
    standardDeduction += standardDeductionPerDependent * numDependents;
    standardDeduction += standardDeductionPerChild * numChildren;
    return standardDeduction;
}

/**
 * Displays the overall cost and savings of M4A to the user on the webpage 
 * @param {number} overallCostOfM4A the overall cost of M4A for the user
 * @param {number} prescriptionCostsUnderM4A the overall cost of prescriptions for the user
 * @param {number} savingsOfM4A the overall savings under M4A for the user
 */
function displayM4ACosts(overallCostOfM4A, prescriptionCostsUnderM4A, savingsOfM4A) { 
    //get the monthly savings 
    var runningCostOfM4AMonthly = overallCostOfM4A / 12;

    //display the total cost of M4A: 
    M4ACalcDisplayElements.healthcareCosts.innerHTML = formatCurrency(overallCostOfM4A + "") + " per year";

    //set the prescription costs under M4A: 
    M4ACalcDisplayElements.prescriptionCosts.innerHTML = formatCurrency(prescriptionCostsUnderM4A + "") + " per year";

    //if the savings are negative, move the negative sign before the number and change the  
    if(savingsOfM4A < 0) { 
        M4ACalcDisplayElements.healthcareSavings.innerHTML = "-" + formatCurrency(savingsOfM4A * -1 + "") + " per year";
    } else { 
        M4ACalcDisplayElements.healthcareSavings.innerHTML = formatCurrency(savingsOfM4A + "") + " per year";
    }

    //if the savings are positive, remove the cost class from the savings element; otherwise, add it: 
    M4ACalcDisplayElements.healthcareSavings.classList.toggle("cost", savingsOfM4A < 0);
}

/**
 * Adds $ sign and commas in typical US-format to the specified input box if it's a money input box
 * @param {DOM Element} inputBoxToFormat the input box that we want to try and format as money
 */
function formatInputValueAsCurrency(inputBoxToFormat) { 
    var isInputBoxMoneyInput = userCurrencyInputs[inputBoxToFormat.name];
    if(isInputBoxMoneyInput != null) { 
        //when we format, the caret might move, so we need to keep track of how much it moves (if it does) so we can move it back to the right spot:
        //get the current caret position and length before formatting:
        var caretPosition = inputBoxToFormat.selectionStart;
        var lengthBeforeFormatting = inputBoxToFormat.value.length;

        //format the input field
        inputBoxToFormat.value = formatCurrency(inputBoxToFormat.value);

        //to move the caret back to the right spot, find out how much the string grew or shrank, then add that to the original caret position
        var lengthAfterFormatting = inputBoxToFormat.value.length;
        caretPosition = lengthAfterFormatting - lengthBeforeFormatting + caretPosition;
        //move the caret back to where it was before we changed the value: 
        inputBoxToFormat.selectionStart = caretPosition;
        inputBoxToFormat.selectionEnd = caretPosition;
    }
}

/**
 * ensures that an input is a number greater than 0
 * @return false if an input has invalid data, true otherwise 
 */
function validateCurrencyInput(inputToValidate) {   
    //check validity using input's validity check: 
    if(!inputToValidate.checkValidity()) { 
        return false;
    }

    //sanitze the value we want to validate
    var valueToParse = convertCurrencyToInt(inputToValidate.value);
    //check if the value is actually a number: 
    var parsedValue = parseFloat(valueToParse);
    if(parsedValue == NaN) { 
        return false;
    }

    //check if the value is negative:
    if(parsedValue < 0) { 
        return false;
    }

    return true;
}

/**
 * Formats a given number, eg. 1000, into a currency format, eg. $1,000
 * @param {number} numberToFormat number to be formatted into a currency format 
 * @return the number formatted as a currency 
 */
function formatCurrency(numberToFormat) { 
    if(numberToFormat === null) { 
        return "";
    }

    //sanitize the value we want to validate
    var sanitizedCurrency = convertCurrencyToInt(numberToFormat);

    //inserts a ',' after every three digits
    var formattedCurrency = sanitizedCurrency.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    
    return "$" + formattedCurrency;
}

/**
 * Removes any commas and $s from a number
 * @param {number} numberToSanitize the number that should have commas and $s removed
 * @return the number with all of its commas and $s removed 
 */
function convertCurrencyToInt(numberToSanitize) { 
    return numberToSanitize.replace(/[,$]/g, "");
}

/**
 * Returns the value of the currently selected radio button in the provided radio button group 
 * @param {element group} radioButtonGroupToCheck group that we should find the selected button for
 * @return returns the value of the selected radio button if one is selected. Otherwise, returns 'ERROR'
 */
function getSelectedRadioButtonValue(radioButtonGroupToCheck) { 
    for(let i = 0; i < radioButtonGroupToCheck.length; i++) { 
        if(radioButtonGroupToCheck[i].checked) { 
            return radioButtonGroupToCheck[i].value;
        }
    }
    return 'ERROR';
}